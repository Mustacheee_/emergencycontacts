package co.jbink.mustacheee.emergencycontacts;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * A placeholder fragment containing a simple view.
 */
public class SendContactsFragment extends Fragment {
    EditText contact1, contact2;
    Button sendBtn;

    public SendContactsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_send_contacts, container, false);

        this.contact1 = (EditText) rootView.findViewById(R.id.send_contact1);
        this.contact2 = (EditText) rootView.findViewById(R.id.send_contact2);
        this.sendBtn = (Button) rootView.findViewById(R.id.send_sendBtn);


        return rootView;
    }
}
