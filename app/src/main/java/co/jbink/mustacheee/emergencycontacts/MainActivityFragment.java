package co.jbink.mustacheee.emergencycontacts;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private Button sendBtn, receiveBtn;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View rootView =  inflater.inflate(R.layout.fragment_main, container, false);

        this.sendBtn = (Button) rootView.findViewById(R.id.main_sendBtn);
        this.receiveBtn = (Button) rootView.findViewById(R.id.main_receiveBtn);

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), SendContacts.class);
                startActivity(i);
            }
        });

        return rootView;
    }
}
